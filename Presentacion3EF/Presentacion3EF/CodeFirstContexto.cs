﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion3EF
{
    public class CodeFirstContexto : DbContext
    {
        #region Propiedades
        public DbSet<Empleado> Empleados { get; set; }
        #endregion

        #region Constructor
        public CodeFirstContexto()
            : base("name=defaultConnection")
        {

        }
        #endregion

        #region Metodos
        #endregion
    }
}
