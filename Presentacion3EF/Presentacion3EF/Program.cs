﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion3EF
{
    class Program
    {
        static void Main(string[] args)
        {
            CodeFirstContexto contexto = new CodeFirstContexto();
            Empleado empleado = new Empleado();
            empleado.Apellidos = "Hernandez Hernandez";
            empleado.Nombres = "Marcos";
            empleado.Clave = "001";
            contexto.Empleados.Add(empleado);
            contexto.SaveChanges();
        }
    }
}
