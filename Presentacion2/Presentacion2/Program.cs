﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion2
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 2;
            decimal precio = 0.1789m;
            float x = 0.3f;
            double doble = 0.666666666d;
            bool bandera = true;
            string cadena = "Hola";
            DateTime fecha = DateTime.MinValue;
            Console.WriteLine("El valor de i es : {0}", i);
            Console.WriteLine("El valor de precio es : {0:C}", precio);
            Console.WriteLine("El valor de x es : {0}", x);
            Console.WriteLine("El valor de doble es : {0}", doble);
            Console.WriteLine("El valor de bandera es : " + bandera);
            Console.WriteLine("El valor de cadena es : " + cadena);
            Console.WriteLine("El valor de fecha es : " + fecha.ToLongDateString());
            i = (int)x;
            Console.WriteLine("El valor de i es : {0}", i);
            i = Convert.ToInt32(x);
            Console.WriteLine("El valor de i es : {0}", i);
            Console.WriteLine("El valor de i es : " + i.ToString());
            Console.WriteLine("El valor de i + 5 es : " + (i + 5).ToString());
            Console.ReadKey();
        }
    }
}
