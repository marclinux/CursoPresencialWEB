//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Presentacion1EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Personas
    {
        public int IdPersona { get; set; }
        public string NombresPersona { get; set; }
        public string ApellidosPersona { get; set; }
        public string LugarNacimiento { get; set; }
        public Nullable<System.DateTime> FechaNacimiento { get; set; }
        public Nullable<int> NumeroRegistroCivil { get; set; }
        public Nullable<int> ActaRegistroCivil { get; set; }
        public Nullable<System.DateTime> FechaAltaRegistroCivil { get; set; }
        public Nullable<int> LibroRegistroCivil { get; set; }
        public string NombrePadre { get; set; }
        public string NombreMadre { get; set; }
        public string NombreAbueloPaterno { get; set; }
        public string NombreAbuelaPaterna { get; set; }
        public string NombreAbueloMaterno { get; set; }
        public string NombreAbuelaMaterna { get; set; }
        public Nullable<bool> Activo { get; set; }
        public Nullable<int> idTipoHijo { get; set; }
    }
}
