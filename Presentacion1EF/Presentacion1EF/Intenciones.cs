//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Presentacion1EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Intenciones
    {
        public int idIntencion { get; set; }
        public Nullable<System.DateTime> FechaHoraIntencion { get; set; }
        public Nullable<int> idTipoIntencion { get; set; }
        public string Nombre { get; set; }
        public string Observaciones { get; set; }
        public Nullable<int> idRecibo { get; set; }
        public Nullable<bool> Agendada { get; set; }
        public Nullable<int> idSacramento { get; set; }
        public Nullable<int> TipoSacramento { get; set; }
    }
}
