﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    public class RepositorioAsignaPlaza: intRepositorioAsignaPlaza, IDisposable
    {
        #region Propiedades
        CodeFirstContexto contexto;
        #endregion

        #region Constructor
        public RepositorioAsignaPlaza(CodeFirstContexto pContexto)
        {
            contexto = pContexto;
        }
        #endregion

        #region Métodos
        public void AgregaAsignaPlaza(AsignaPlaza asignaplaza)
        {
            contexto.AsignacionesPlaza.Add(asignaplaza);
        }
        public bool ModificaAsignaPlaza(AsignaPlaza asignaplaza)
        {
            bool regresa = false;
            AsignaPlaza oAsignaPlaza;
            oAsignaPlaza = contexto.AsignacionesPlaza.Find(asignaplaza.AsignaPlazaId);
            if (oAsignaPlaza != null)
            {
                oAsignaPlaza.FechaInicio = asignaplaza.FechaInicio;
                oAsignaPlaza.FechaFinal = asignaplaza.FechaFinal;
                oAsignaPlaza.EmpleadoId = asignaplaza.EmpleadoId;
                oAsignaPlaza.PlazaId = asignaplaza.PlazaId;
                contexto.Entry(oAsignaPlaza).State = System.Data.Entity.EntityState.Modified;
            }
            return regresa;
        }
        public bool EliminaAsignaPlaza(int id)
        {
            bool regresa = false;
            AsignaPlaza oAsignaPlaza;
            oAsignaPlaza = contexto.AsignacionesPlaza.Find(id);
            if (oAsignaPlaza != null)
            {
                contexto.AsignacionesPlaza.Remove(oAsignaPlaza);
            }
            return regresa;
        }
        public AsignaPlaza getAsignaPlaza(int id)
        {
            AsignaPlaza oAsignaPlaza = contexto.AsignacionesPlaza.Find(id);
            if (oAsignaPlaza != null)
            {
                contexto.Entry(oAsignaPlaza).Reference("oEmpleado").Load();
                contexto.Entry(oAsignaPlaza).Reference("oPlaza").Load();
            }
            return oAsignaPlaza;
        }
        public ICollection<AsignaPlaza> getAsignaPlazas() 
        {
            var lista = (from ap in contexto.AsignacionesPlaza
                         orderby ap.FechaInicio
                         select ap).ToList();
            return lista;
        }

        public void Guardar()
        {
            contexto.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    contexto.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
