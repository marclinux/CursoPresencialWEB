﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    [Table("AsignaPlazas")]
    public class AsignaPlaza
    {
        #region Propiedades
        public int AsignaPlazaId { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFinal { get; set; }
        public int EmpleadoId { get; set; }
        public int PlazaId { get; set; }
        public virtual Empleado oEmpleado { get; set; }
        public virtual Plaza oPlaza { get; set; }
        #endregion

        #region Constructor
        #endregion

        #region Metodos
        #endregion
    }
}
