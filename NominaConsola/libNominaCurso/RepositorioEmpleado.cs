﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    public class RepositorioEmpleado : IntRepositorioEmpleado, IDisposable
    {
        #region Propiedades
        CodeFirstContexto contexto;
        #endregion

        #region Constructor
        public RepositorioEmpleado(CodeFirstContexto pContexto)
        {
            contexto = pContexto;
        }
        #endregion

        #region Metodos
        public void AgregaEmpleado(Empleado empleado)
        {
            contexto.Empleados.Add(empleado);
        }

        public bool ModificaEmpleado(Empleado empleado)
        {
            bool regresa = false;
            Empleado oEmpleado;
            oEmpleado = contexto.Empleados.Find(empleado.EmpleadoId);
            if(oEmpleado != null)
            {
                oEmpleado.Nombres = empleado.Nombres;
                oEmpleado.Apellidos = empleado.Apellidos;
                oEmpleado.Clave = empleado.Clave;
                contexto.Entry(oEmpleado).State = System.Data.Entity.EntityState.Modified;
                regresa = true;
            }
            return regresa;
        }

        public bool EliminaEmpleado(int id)
        {
            bool regresa = false;
            Empleado oEmpleado;
            oEmpleado = contexto.Empleados.Find(id);
            if(oEmpleado != null)
            {
                contexto.Entry(oEmpleado).Collection("Beneficiarios").Load();
                contexto.Empleados.Remove(oEmpleado);
                regresa = true;
            }
            return regresa;
        }

        public Empleado GetEmpleado(int id)
        {
            Empleado empleado = contexto.Empleados.Find(id);
            if(empleado != null)
            {
                contexto.Entry(empleado).Collection("Beneficiarios").Load();
            }
            return empleado;
        }

        public ICollection<Empleado> GetEmpleados()
        {
            var lista = (from e in contexto.Empleados
                         orderby e.Apellidos select e).ToList();
            return lista;
        }

        public void Guardar()
        {
            contexto.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    contexto.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
