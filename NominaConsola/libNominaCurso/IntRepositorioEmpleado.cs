﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    public interface IntRepositorioEmpleado
    {
        void AgregaEmpleado(Empleado empleado);
        bool ModificaEmpleado(Empleado empleado);
        bool EliminaEmpleado(int id);
        Empleado GetEmpleado(int id);
        ICollection<Empleado> GetEmpleados();
        void Guardar();
    }
}
