﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    [Table("Plazas")]
    public class Plaza
    {
        #region Propiedades
        public int PlazaId { get; set; }
        public string Clave { get; set; }
        public string NombrePlaza { get; set; }
        public virtual ICollection<AsignaPlaza> AsignacionesPlaza  { get; set; }
        #endregion

        #region Constructor
        #endregion

        #region Metodos
        #endregion
    }
}
