﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    public interface intRepositorioAsignaPlaza
    {
        void AgregaAsignaPlaza(AsignaPlaza asignaplaza);
        bool ModificaAsignaPlaza(AsignaPlaza asignaplaza);
        bool EliminaAsignaPlaza(int id);
        AsignaPlaza getAsignaPlaza(int id);
        ICollection<AsignaPlaza> getAsignaPlazas();
        void Guardar();
    }
}
