﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    [Table("Beneficiarios")]
    public class Beneficiario
    {
        #region Propiedades
        public int BeneficiarioId { get; set; }
        [MaxLength(200), Required]
        public string NombreCompleto { get; set; }
        public decimal Porcentaje { get; set; }
        public int EmpleadoId { get; set; }
        public virtual Empleado EmpleadoBeneficiario { get; set; }
        #endregion

        #region Constructor
        #endregion

        #region Metodos
        #endregion
    }
}
