﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    public class CodeFirstContexto : IdentityDbContext<ApplicationUser>
    {
        #region Propiedades
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Beneficiario> Beneficiarios { get; set; }
        public DbSet<Plaza> Plazas { get; set; }
        public DbSet<AsignaPlaza> AsignacionesPlaza { get; set; }
        #endregion

        #region Constructor
        public CodeFirstContexto()
            : base("name=defaultConnection")
        {
        }
        #endregion

        #region Metodos
        public static CodeFirstContexto Create()
        {
            return new CodeFirstContexto();
        }
        #endregion
    }
}
