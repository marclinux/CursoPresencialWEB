﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libNominaCurso
{
    [Table("Empleados")]
    public class Empleado
    {
        #region Propiedades
        public int EmpleadoId { get; set; }
        public string Clave { get; set; }
        [MaxLength(100), Required]
        public string Nombres { get; set; }
        [MaxLength(100), Required]
        public string Apellidos { get; set; }
        public virtual ICollection<Beneficiario> Beneficiarios { get; set; }
        public virtual ICollection<AsignaPlaza> AsignacionesPlaza { get; set; }
        #endregion

        #region Constructor
        #endregion

        #region Metodos
        #endregion
    }
}
