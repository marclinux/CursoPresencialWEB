namespace libNominaCurso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Plazas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AsignaPlazas",
                c => new
                    {
                        AsignaPlazaId = c.Int(nullable: false, identity: true),
                        FechaInicio = c.DateTime(nullable: false),
                        FechaFinal = c.DateTime(),
                        EmpleadoId = c.Int(nullable: false),
                        PlazaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AsignaPlazaId)
                .ForeignKey("dbo.Empleados", t => t.EmpleadoId, cascadeDelete: false)
                .ForeignKey("dbo.Plazas", t => t.PlazaId, cascadeDelete: false)
                .Index(t => t.EmpleadoId)
                .Index(t => t.PlazaId);
            
            CreateTable(
                "dbo.Plazas",
                c => new
                    {
                        PlazaId = c.Int(nullable: false, identity: true),
                        Clave = c.String(),
                        NombrePlaza = c.String(),
                    })
                .PrimaryKey(t => t.PlazaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AsignaPlazas", "PlazaId", "dbo.Plazas");
            DropForeignKey("dbo.AsignaPlazas", "EmpleadoId", "dbo.Empleados");
            DropIndex("dbo.AsignaPlazas", new[] { "PlazaId" });
            DropIndex("dbo.AsignaPlazas", new[] { "EmpleadoId" });
            DropTable("dbo.Plazas");
            DropTable("dbo.AsignaPlazas");
        }
    }
}
