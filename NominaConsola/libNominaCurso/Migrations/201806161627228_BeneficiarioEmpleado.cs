namespace libNominaCurso.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BeneficiarioEmpleado : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Beneficiarios",
                c => new
                    {
                        BeneficiarioId = c.Int(nullable: false, identity: true),
                        NombreCompleto = c.String(nullable: false, maxLength: 200),
                        Porcentaje = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EmpleadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BeneficiarioId)
                .ForeignKey("dbo.Empleados", t => t.EmpleadoId, cascadeDelete: false)
                .Index(t => t.EmpleadoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Beneficiarios", "EmpleadoId", "dbo.Empleados");
            DropIndex("dbo.Beneficiarios", new[] { "EmpleadoId" });
            DropTable("dbo.Beneficiarios");
        }
    }
}
