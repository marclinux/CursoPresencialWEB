﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libNominaCurso;

namespace NominaConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            CodeFirstContexto contexto = new CodeFirstContexto();
            AsignaPlaza asignaplaza = new AsignaPlaza();
            //asignaplaza.FechaInicio = DateTime.Now;
            //asignaplaza.EmpleadoId = 2;
            //asignaplaza.PlazaId = 1;
            //contexto.AsignacionesPlaza.Add(asignaplaza);
            asignaplaza = contexto.AsignacionesPlaza.Find(1);
            contexto.Entry(asignaplaza).Reference("oEmpleado").Load();
            contexto.Entry(asignaplaza).Reference("oPlaza").Load();
            Console.WriteLine("El empleado {0} fue asignado a la plaza {1}", asignaplaza.oEmpleado.Nombres +
                            " " + asignaplaza.oEmpleado.Apellidos, asignaplaza.oPlaza.NombrePlaza);
            Console.ReadKey();
            //Empleado empleado = contexto.Empleados.Find(1);
            //if(empleado != null)
            //{
            //    contexto.Entry(empleado).Collection("Beneficiarios").Load();
            //    contexto.Empleados.Remove(empleado);
            //    contexto.SaveChanges();
            //}
            //Empleado empleado = new Empleado();
            //empleado.Nombres = "Pedro";
            //empleado.Apellidos = "Pérez";
            //empleado.Clave = "001";
            //Beneficiario beneficiario = new Beneficiario();
            //beneficiario.NombreCompleto = "Uriel Hernández Camacho";
            //beneficiario.Porcentaje = 15.0M;
            //empleado.Beneficiarios = new List<Beneficiario>();
            //empleado.Beneficiarios.Add(beneficiario);
            //beneficiario = new Beneficiario();
            //beneficiario.NombreCompleto = "Leticia Camacho";
            //beneficiario.Porcentaje = 85.0M;
            //empleado.Beneficiarios.Add(beneficiario);
            //contexto.Empleados.Add(empleado);
            //Plaza plaza = new Plaza();
            //plaza.Clave = "P001";
            //plaza.NombrePlaza = "ANALISTA A";
            //contexto.Plazas.Add(plaza);
            contexto.SaveChanges();
        }
    }
}
