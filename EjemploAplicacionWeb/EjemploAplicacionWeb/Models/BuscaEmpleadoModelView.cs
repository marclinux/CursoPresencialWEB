﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using libNominaCurso;
using System.ComponentModel.DataAnnotations;

namespace EjemploAplicacionWeb.Models
{
    public class BuscaEmpleadoModelView
    {
        #region Propiedades
        public IEnumerable<Empleado> Empleados { get; set; }
        [Required]
        public string cadenaBusqueda { get; set; }
        #endregion

        #region Constructor
        public BuscaEmpleadoModelView()
        {
            Empleados = new List<Empleado>();
        }
        #endregion
    }
}