﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using libNominaCurso;

namespace EjemploAplicacionWeb.Controllers
{
    public class AsignaPlazasController : Controller
    {
        private CodeFirstContexto db = new CodeFirstContexto();

        // GET: AsignaPlazas
        public ActionResult Index()
        {
            var asignacionesPlaza = db.AsignacionesPlaza.Include(a => a.oEmpleado).Include(a => a.oPlaza);
            return View(asignacionesPlaza.ToList());
        }

        // GET: AsignaPlazas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AsignaPlaza asignaPlaza = db.AsignacionesPlaza.Find(id);
            if (asignaPlaza == null)
            {
                return HttpNotFound();
            }
            return View(asignaPlaza);
        }

        // GET: AsignaPlazas/Create
        public ActionResult Create()
        {
            ViewBag.EmpleadoId = new SelectList(db.Empleados, "EmpleadoId", "Nombres");
            ViewBag.PlazaId = new SelectList(db.Plazas, "PlazaId", "NombrePlaza");
            return View();
        }

        // POST: AsignaPlazas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AsignaPlazaId,FechaInicio,FechaFinal,EmpleadoId,PlazaId")] AsignaPlaza asignaPlaza)
        {
            if (ModelState.IsValid)
            {
                db.AsignacionesPlaza.Add(asignaPlaza);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EmpleadoId = new SelectList(db.Empleados, "EmpleadoId", "Nombres", asignaPlaza.EmpleadoId);
            ViewBag.PlazaId = new SelectList(db.Plazas, "PlazaId", "NombrePlaza", asignaPlaza.PlazaId);
            return View(asignaPlaza);
        }

        // GET: AsignaPlazas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AsignaPlaza asignaPlaza = db.AsignacionesPlaza.Find(id);
            if (asignaPlaza == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmpleadoId = new SelectList(db.Empleados, "EmpleadoId", "Clave", asignaPlaza.EmpleadoId);
            ViewBag.PlazaId = new SelectList(db.Plazas, "PlazaId", "Clave", asignaPlaza.PlazaId);
            return View(asignaPlaza);
        }

        // POST: AsignaPlazas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AsignaPlazaId,FechaInicio,FechaFinal,EmpleadoId,PlazaId")] AsignaPlaza asignaPlaza)
        {
            if (ModelState.IsValid)
            {
                db.Entry(asignaPlaza).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EmpleadoId = new SelectList(db.Empleados, "EmpleadoId", "Clave", asignaPlaza.EmpleadoId);
            ViewBag.PlazaId = new SelectList(db.Plazas, "PlazaId", "Clave", asignaPlaza.PlazaId);
            return View(asignaPlaza);
        }

        // GET: AsignaPlazas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AsignaPlaza asignaPlaza = db.AsignacionesPlaza.Find(id);
            if (asignaPlaza == null)
            {
                return HttpNotFound();
            }
            return View(asignaPlaza);
        }

        // POST: AsignaPlazas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AsignaPlaza asignaPlaza = db.AsignacionesPlaza.Find(id);
            db.AsignacionesPlaza.Remove(asignaPlaza);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
