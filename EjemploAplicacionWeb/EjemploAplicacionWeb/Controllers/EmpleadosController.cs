﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using libNominaCurso;
using EjemploAplicacionWeb.Models;

namespace EjemploAplicacionWeb.Controllers
{
    public class EmpleadosController : Controller
    {

        private CodeFirstContexto db;
        private RepositorioEmpleado repoEmpleado;
        public EmpleadosController()
        {
            db = new CodeFirstContexto();
            repoEmpleado = new RepositorioEmpleado(db);
        }

        // GET: Empleados
        public ActionResult Index()
        {
            return View(repoEmpleado.GetEmpleados());
        }

        // GET: Empleados
        public ActionResult Busca()
        {
            BuscaEmpleadoModelView busca = new BuscaEmpleadoModelView();
            return View(busca);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BuscaNombre([Bind(Include = "cadenaBusqueda")] 
                                            BuscaEmpleadoModelView busca)
        {
            busca.Empleados = repoEmpleado.GetEmpleados(busca.cadenaBusqueda);
            return PartialView("_ListaEmpleados", busca.Empleados);
        }

        // GET: Empleados/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = repoEmpleado.GetEmpleado((int)id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // GET: Empleados/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Empleados/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmpleadoId,Clave,Nombres,Apellidos")] Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                repoEmpleado.AgregaEmpleado(empleado);
                repoEmpleado.Guardar();
                return RedirectToAction("Index");
            }

            return View(empleado);
        }

        // GET: Empleados/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = repoEmpleado.GetEmpleado((int)id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // POST: Empleados/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmpleadoId,Clave,Nombres,Apellidos")] Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                repoEmpleado.ModificaEmpleado(empleado);
                repoEmpleado.Guardar();
                return RedirectToAction("Index");
            }
            return View(empleado);
        }

        // GET: Empleados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = repoEmpleado.GetEmpleado((int)id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // POST: Empleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            repoEmpleado.EliminaEmpleado(id);
            repoEmpleado.Guardar();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                repoEmpleado.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
