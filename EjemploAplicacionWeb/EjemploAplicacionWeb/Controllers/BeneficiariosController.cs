﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using libNominaCurso;

namespace EjemploAplicacionWeb.Controllers
{
    public class BeneficiariosController : Controller
    {
        private CodeFirstContexto db = new CodeFirstContexto();

        // GET: Beneficiarios/Create
        public ActionResult Create(int id)
        {
            Beneficiario beneficiario = new Beneficiario();
            beneficiario.EmpleadoId = id;
            return PartialView("_CreaBeneficiario", beneficiario);
        }

        // POST: Beneficiarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BeneficiarioId,NombreCompleto,Porcentaje,EmpleadoId")] Beneficiario beneficiario)
        {
            if (ModelState.IsValid)
            {
                db.Beneficiarios.Add(beneficiario);
                db.SaveChanges();
                return Json(new { success = true, id = beneficiario.EmpleadoId });
            }

            return PartialView("_CreaBeneficiario", beneficiario);
        }

        // GET: Beneficiarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Beneficiario beneficiario = db.Beneficiarios.Find(id);
            if (beneficiario == null)
            {
                return HttpNotFound();
            }
            return PartialView("_CreaBeneficiario", beneficiario);
        }

        // POST: Beneficiarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BeneficiarioId,NombreCompleto,Porcentaje,EmpleadoId")] Beneficiario beneficiario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(beneficiario).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true, id = beneficiario.EmpleadoId });
            }
            return PartialView("_CreaBeneficiario", beneficiario);
        }

        // GET: Beneficiarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Beneficiario beneficiario = db.Beneficiarios.Find(id);
            if (beneficiario == null)
            {
                return HttpNotFound();
            }
            return PartialView("_EliminaBeneficiario", beneficiario);
        }

        // POST: Beneficiarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed([Bind(Include = "BeneficiarioId,NombreCompleto,Porcentaje,EmpleadoId")] Beneficiario beneficiario)
        {
            Beneficiario bdbeneficiario = db.Beneficiarios.Find(beneficiario.BeneficiarioId);
            db.Beneficiarios.Remove(bdbeneficiario);
            db.SaveChanges();
            return Json(new { success = true, id = beneficiario.EmpleadoId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
