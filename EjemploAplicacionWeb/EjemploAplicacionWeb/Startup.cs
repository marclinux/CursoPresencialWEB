﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EjemploAplicacionWeb.Startup))]
namespace EjemploAplicacionWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
