﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapacitacionCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            do
            {

                Console.WriteLine("0. Salir");
                Console.WriteLine("1. Arreglos");
                Console.WriteLine("2. Colecciones");
                // Agregado por JP
                Console.WriteLine("3. Clase Equipos");
                // Agregado por AL
                Console.WriteLine("4.- Colecciones de la Clase Productos");
                // Agregado por MS
                Console.WriteLine("5. Usuarios");
                Console.WriteLine("Dame la opcion deseada:");
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                    case 1: Arreglos();
                        break;
                    case 2: Listas();
                        break;
                    case 3:
                        claseEquipos();
                        break;
                    case 4: Productos();
			break;
                    case 5: Usuarios();
                        break;
                    default:
                        break;
                }
            } while (opcion != 0);
        }

        static void Arreglos()
        {
            clsArreglos Arreglo = new clsArreglos();
            int opcion = 0;
            do
            {
                Console.WriteLine("0. Salir");
                Console.WriteLine("1. Agregar");
                Console.WriteLine("2. Eliminar");
                Console.WriteLine("3. Imprimir");
                Console.WriteLine("Dame la opcion deseada:");
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Dame el valor a agregar al arreglo:");
                        Arreglo.Agrega(Console.ReadLine());
                        break;
                    case 2: Console.WriteLine("Dame la posicion del elemento a eliminar:");
                        Arreglo.Eliminar(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case 3:
                        Console.WriteLine(Arreglo.Imprimir());
                        Console.ReadKey();
                        break;
                    default:
                        break;
                }
            } while (opcion != 0);
        }

        static void Listas()
       {
           int opcion = 0;
           List<string> Lista = new List<string>();
           do
           {
               Console.WriteLine("0. Salir");
               Console.WriteLine("1. Agregar");
               Console.WriteLine("2. Eliminar");
               Console.WriteLine("3. Imprimir");
               Console.WriteLine("Dame la opcion deseada:");
               opcion = Convert.ToInt32(Console.ReadLine());
               switch (opcion)
               {
                   case 1:
                       Console.WriteLine("Dame el valor a agregar al arreglo:");
                       Lista.Add(Console.ReadLine());
                       break;
                   case 2: Console.WriteLine("Dame la posicion del elemento a eliminar:");
                       Lista.RemoveAt(Convert.ToInt32(Console.ReadLine()));
                       break;
                   case 3:
                       string listaConComas = "";
                       bool inicio= true;
                       foreach (var item in Lista)
                       {
                           if (inicio)
                           {
                               listaConComas += item;
                               inicio = false;
                           }
                           else
                               listaConComas += ", " + item;
                       }
                       //for (int i = 0; i < Lista.Count; i++)
                       //{
                       //    if (i == 0)
                       //        listaConComas += Lista[i];
                       //    else
                       //        listaConComas += ", " + Lista[i];
                       //}
                       Console.WriteLine(listaConComas);
                       Console.ReadKey();
                       break;
                   default:
                       break;
               }
           } while (opcion != 0);
       }

        //Clase Equipos
        static void claseEquipos()
        {
            List<clsEquipos> Equipos = new List<clsEquipos>();

            clsEquipos Equipo = new clsEquipos();

            int opcion = 0;
            do
            {
                Console.WriteLine("0. Salir");
                Console.WriteLine("1. Agregar");
                Console.WriteLine("2. Eliminar");
                Console.WriteLine("3. Imprimir");
                Console.WriteLine("Dame la opcion deseada:");
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Dame el id del equipo: ");
                        Equipo.id = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("Dame la descripción del equipo: ");
                        Equipo.descripcion = Console.ReadLine();

                        Console.WriteLine("Dame la marca del equipo: ");
                        Equipo.marca = Console.ReadLine();

                        Console.WriteLine("Dame el modelo del equipo: ");
                        Equipo.modelo = Console.ReadLine();

                        Equipos.Add(Equipo);
                        break;
                    case 2:
                        Console.WriteLine("Dame el la posicion del equipo a eliminar:");
                        Equipos.RemoveAt(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case 3:
                        foreach (var item in Equipos)
                        {
                            Console.WriteLine("Id: {0}, Descripcion: {1}, Marca: {2}, Modelo: {3}", item.id, item.descripcion, item.marca, item.modelo);
                        }
                        Console.ReadKey();
                        break;
                    default:
                        break;
                }
            } while (opcion != 0);
        }

        static void Productos()
        {
            int opcion = 0;
            int id;
            string descripcion, tipo, empresaElaboro;
            DateTime fechaElaboracion;
            List<clsProductos> misProductos = new List<clsProductos>();

            do
            {
                Console.WriteLine("0. Salir");
                Console.WriteLine("1. Agregar Producto");
                Console.WriteLine("2. Eliminar Producto");
                Console.WriteLine("3. Imprimir Producto");
                Console.WriteLine("Dame la opcion deseada:");
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Ingresa el ID del producto");
                        id = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Ingresa la descripcion del producto");
                        descripcion = Console.ReadLine();
                        Console.WriteLine("Ingresa el tipo del producto");
                        tipo = Console.ReadLine();
                        Console.WriteLine("Ingresa la fecha de elaboración del producto");
                        fechaElaboracion = Convert.ToDateTime(Console.ReadLine());
                        Console.WriteLine("Ingresa el nombre de la empresa que elaboró el producto");
                        empresaElaboro = Console.ReadLine();

                        clsProductos miproducto = new clsProductos(id, descripcion, tipo, fechaElaboracion, empresaElaboro);
                        misProductos.Add(miproducto);

                        break;
                    case 2:
                        Console.WriteLine("Dame la posicion del elemento a eliminar:");
                        misProductos.RemoveAt(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case 3:
                        foreach (var item in misProductos)
                        {
                            Console.WriteLine("ID: {0}, Descripcion: {1}, Tipo: {2}, Fecha Elaboración: {3}, Empresa Elaboró {4}", item.Id, item.descripcion, item.tipo, item.fechaElaboracion, item.empresaElaboro);
                        }
                        Console.ReadKey();
                        break;
                    default:

                        break;
                }
            } while (opcion != 0);
	}
        static void Usuarios()
        {
            int opcion = 0;
            List<clsUsuarios> Lista = new List<clsUsuarios>();
            do
            {
                Console.WriteLine("0. Salir");
                Console.WriteLine("1. Agregar");
                Console.WriteLine("2. Eliminar");
                Console.WriteLine("3. Imprimir");
                Console.WriteLine("Dame la opcion deseada:");
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        clsUsuarios Usuario = new clsUsuarios();
                        Console.WriteLine("Dame el id del usuario:");
                        Usuario.id = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Dame el nombre del usuario:");
                        Usuario.Nombre = Console.ReadLine();
                        Console.WriteLine("Dame la dirección del usuario:");
                        Usuario.Direccion = Console.ReadLine();
                        Console.WriteLine("Dame el login del usuario:");
                        Usuario.Login = Console.ReadLine();
                        Console.WriteLine("Dame el password del usuario:");
                        Usuario.Password = Console.ReadLine();
                        Console.WriteLine("Fecha del último acceso del usuario:");
                        Usuario.FechaUltimoAcceso = Convert.ToDateTime(Console.ReadLine());
                        Usuario.FechaCreacion = DateTime.Now;
                        Lista.Add(Usuario);
                        break;
                    case 2: Console.WriteLine("Dame la posicion del elemento a eliminar:");
                        int posicion = Convert.ToInt32(Console.ReadLine());
                        if (posicion <= Lista.Count - 1)
                            Lista.RemoveAt(posicion);
                        else
                        {
                            Console.WriteLine("No existe elemento en la posición indicada");
                            Console.ReadLine();
                        }
                        break;
                    case 3:
                        foreach (var item in Lista)
                        {
                            Console.WriteLine("Id: {0}, Nombre: {1}, Dirección: {2}, FechaCreación: {3}, Fecha Ultimo Acceso: {4}, login: {5}, " +
                            "password: {6}", item.id, item.Nombre, item.Direccion, item.FechaCreacion.ToString(), item.FechaUltimoAcceso, 
                            item.Login, item.Password);
                        }
                        if (Lista.Count == 0)
                            Console.WriteLine("La lista no tiene elementos");
                        Console.ReadKey();
                        break;
                    default:
                        break;
                }
            } while (opcion != 0);
        }
    }
}
