﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapacitacionCSharp
{
    public class clsProductos
    {
        #region Propiedades
        public int Id { get; set; }
        public string descripcion { get; set; }
        public string tipo { get; set; }
        public DateTime fechaElaboracion { get; set; }
        public string empresaElaboro { get; set; }
        #endregion

        #region Constructor
        public clsProductos(int id, string descripcion, string tipo, DateTime fechaElaboracion, string empresaElaboro)
        {
            this.Id = id;
            this.descripcion = descripcion;
            this.tipo = tipo;
            this.fechaElaboracion = fechaElaboracion;
            this.empresaElaboro = empresaElaboro;
        }
        #endregion

        #region Metodos    
        #endregion
    }
}
