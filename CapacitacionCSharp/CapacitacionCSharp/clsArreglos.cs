﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapacitacionCSharp
{
    public class clsArreglos
    {
        #region Propiedades
        public string[] Arreglo { get; set; }
        private int indice = 0;
        #endregion

        #region Constructor
        public clsArreglos()
        {
            Arreglo = new string[20];
        }
        #endregion

        #region Metodos
        public void Agrega(string valor)
        {
            if (indice < 20)
            {
                Arreglo[indice] = valor;
                indice++;
            }
        }

        public void Eliminar(int posicion)
        {
            if(posicion < indice)
            {
                for (int i = posicion; i < indice - 1; i++)
                {
                    Arreglo[i] = Arreglo[i + 1];
                }
                indice--;
            }
        }

        public string Imprimir()
        {
            string cadenaRegresa = "";
            for (int i = 0; i < indice; i++)
            {
                if (i == 0)
                    cadenaRegresa += Arreglo[i];
                else
                    cadenaRegresa += ", " + Arreglo[i];
            }
            return cadenaRegresa;
        }
        #endregion
    }
}
