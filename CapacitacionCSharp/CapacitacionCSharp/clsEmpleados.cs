﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapacitacionCSharp
{
    public class clsEmpleados : clsAbstractaEmpleados, intEmpleados
    {
        #region Propiedades
        public int Id { get; set; }
        public string Clave { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        #endregion

        #region Constructor
        #endregion

        #region Metodos
        public override bool RevisaClave()
        {
            if (Clave == null)
                return false;
            else
                return true;
        }
        #endregion
    }
}
