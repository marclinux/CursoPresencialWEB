﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapacitacionCSharp
{
    public class clsUsuarios
    {
        #region Propiedades
        public int id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaUltimoAcceso { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        #endregion

        #region Constructor
        #endregion

        #region Métodos
        #endregion

        #region Métodos de eventos
        #endregion

    }
}
