﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapacitacionCSharp
{
    public interface intEmpleados
    {
        int Id { get; set; }
        string Clave { get; set; }
        string Nombres { get; set; }
        string Apellidos { get; set; }
    }
}

