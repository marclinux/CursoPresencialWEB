﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapacitacionCSharp
{
    public class clsEmpleadosTrabajo : clsEmpleadosGenerales
    {
        #region Propiedades
        public int Tipo { get; set; }
        public string RFC { get; set; }
        public decimal SalarioMensual { get; set; }
        public string NumeroSeguroSocial { get; set; }
        public int idPuesto { get; set; }
        public string NumeroCuentaBancaria { get; set; }
        public int NumeroDependientesEconomicos { get; set; }
        #endregion

        #region Constructor
        #endregion

        #region Metodos
        #endregion
    }
}
